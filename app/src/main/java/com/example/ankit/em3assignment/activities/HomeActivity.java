package com.example.ankit.em3assignment.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.example.ankit.em3assignment.R;
import com.example.ankit.em3assignment.adapters.PhotosPagerAdapter;
import com.example.ankit.em3assignment.fragments.PhotosFragment;
import com.example.ankit.em3assignment.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    private PhotosPagerAdapter mPhotosPagerAdapter;
    private FloatingActionButton mFab;
    private ViewPager mVpContainer;

    private Uri mCameraPhotoUri;
    private static final int REQUEST_CAMERA = 1000;
    private static final int REQUEST_PERMISSIONS = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //setup toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //calculate screen size (to load image according to screen size)
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        //setup Fab
        mFab = (FloatingActionButton) findViewById(R.id.fab);
        if (mFab != null) {
            mFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startCameraCapture();
                }
            });
        }

        //setup tabs
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        //setup the ViewPager
        mVpContainer = (ViewPager) findViewById(R.id.vp_container);
        if (mVpContainer != null) {

            //set view pager adapter
            mPhotosPagerAdapter = new PhotosPagerAdapter(getSupportFragmentManager(), width, height);
            mVpContainer.setAdapter(mPhotosPagerAdapter);

            //bind view pager to the tabsLayout
            if (tabLayout != null) {
                tabLayout.setupWithViewPager(mVpContainer);
            }

            //set page transform listener to view pager
            mVpContainer.setPageTransformer(false, new ViewPager.PageTransformer() {
                @Override
                public void transformPage(View page, float position) {
                    //animate fab
                    mFab.setRotation(-position * 360);
                }
            });
        }

        //ask for permissions
        checkForPermissions();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CAMERA:
                //send image to the fragment
                sendImageToFragment();
                break;

            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                //validate if permissions are granted or not
                validatePermissionsGrant(permissions, grantResults);
                break;

            default:
                break;
        }
    }

    private void checkForPermissions() {
        List<String> permissions = new ArrayList<>(3);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(android.Manifest.permission.CAMERA);
        }

        if (permissions.size() > 0) {
            ActivityCompat.requestPermissions(this,
                    permissions.toArray(new String[permissions.size()]),
                    REQUEST_PERMISSIONS);
        } else {
            showFab();
        }
    }

    private void validatePermissionsGrant(@NonNull String[] permissions, @NonNull int[] grantResults) {

        boolean allPermissionsGranted = true;

        //if al permissions are granted then show fab button else show toast
        for (int i = 0, length = permissions.length; i < length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, permissions[i] + " is required", Toast.LENGTH_SHORT).show();
                allPermissionsGranted = false;
                break;
            }
        }

        if (allPermissionsGranted) {
            showFab();
        }
    }

    private void showFab() {
        // make the view visible and start the animation
        mFab.setVisibility(View.VISIBLE);

        mFab.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mFab.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                // create the animator for this view (the start radius is zero)
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

                    // get the center for the clipping circle
                    int cx = mFab.getWidth() / 2;
                    int cy = mFab.getHeight() / 2;

                    // get the final radius for the clipping circle
                    float finalRadius = (float) Math.hypot(cx, cy);

                    ViewAnimationUtils.createCircularReveal(mFab, cx, cy, 0, finalRadius).start();
                }
            }
        });
    }

    private void sendImageToFragment() {
        ((PhotosFragment) mPhotosPagerAdapter.getItem(mVpContainer.getCurrentItem()))
                .addPhoto(mCameraPhotoUri.getPath());
    }

    private void startCameraCapture() {
        //get uri of the file in which image is saved
        mCameraPhotoUri = Utils.getSaveImageFileUri();

        if (mCameraPhotoUri != null) {
            //start camera capture intent
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mCameraPhotoUri);
            startActivityForResult(intent, REQUEST_CAMERA);
        } else {
            Toast.makeText(this, "Something went wrong while creating image file", Toast.LENGTH_SHORT).show();
        }
    }
}
