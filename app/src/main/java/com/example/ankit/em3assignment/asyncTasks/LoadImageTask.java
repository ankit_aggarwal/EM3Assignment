package com.example.ankit.em3assignment.asyncTasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.widget.ImageView;

import com.example.ankit.em3assignment.utils.Utils;

import java.lang.ref.WeakReference;

//TODO: not checking for correct orientation
public class LoadImageTask extends AsyncTask<Void, Void, Bitmap> {

    private WeakReference<ImageView> mImageViewWeakReference;
    private String mPath;
    private int mWidth;
    private int mHeight;

    private LruCache<String, Bitmap> mBitmapCache;

    public LoadImageTask(ImageView imageView, String path, int width, int height) {
        mImageViewWeakReference = new WeakReference<>(imageView);
        mPath = path;
        mWidth = width;
        mHeight = height;

        //initialize LRU cache with max size 50;
        mBitmapCache = new LruCache<>(50);
    }

    @Override
    protected Bitmap doInBackground(Void... params) {

        //if bitmap is present in cache then return that instead of decoding it
        Bitmap bitmap = mBitmapCache.get(mPath);

        //if bitmap is null then decode uri to get bitmap
        if (bitmap == null) {

            //initialize bitmap factory options
            BitmapFactory.Options options = new BitmapFactory.Options();

            //decode file in decode bounds to get width and height
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(mPath, options);

            //set sampling size
            options.inSampleSize = Utils.calculateInSampleSize(options, mWidth, mHeight);
            //set inJustDecodeBounds to false for actual decoding
            options.inJustDecodeBounds = false;

            //decode bitmap;
            bitmap = BitmapFactory.decodeFile(mPath, options);

            //put bitmap in cache;
            mBitmapCache.put(mPath, bitmap);
        }

        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {

        //get image view from weak reference and check if its null or not
        ImageView imageView = mImageViewWeakReference.get();

        //if image view is not null then set bitmap in it
        if (imageView != null) {
            imageView.setImageBitmap(bitmap);
        }
    }
}
