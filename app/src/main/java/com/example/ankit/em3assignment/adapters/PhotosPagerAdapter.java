package com.example.ankit.em3assignment.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.ankit.em3assignment.fragments.PhotosFragment;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class PhotosPagerAdapter extends FragmentPagerAdapter {

    private PhotosFragment[] photosFragments = new PhotosFragment[2];

    public PhotosPagerAdapter(FragmentManager fm, int requiredWidth, int requiredHeight) {
        super(fm);

        photosFragments[0] = PhotosFragment.newInstance(requiredWidth, requiredHeight);
        photosFragments[1] = PhotosFragment.newInstance(requiredWidth, requiredHeight);
    }

    @Override
    public Fragment getItem(int position) {
        return photosFragments[position];
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "SECTION 1";
            case 1:
                return "SECTION 2";
        }
        return null;
    }
}
