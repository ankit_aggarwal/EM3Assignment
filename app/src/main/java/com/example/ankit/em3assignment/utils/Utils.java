package com.example.ankit.em3assignment.utils;

import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.File;

public final class Utils {

    private Utils() {
    }

    /**
     * This function calculates the sampling factor
     *
     * @param options        is BitmapFactory options
     * @param requiredWidth  is the required width of the image
     * @param requiredHeight is the required height of the image
     * @return inSampleSize
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int requiredWidth, int requiredHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > requiredHeight || width > requiredWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > requiredHeight
                    && (halfWidth / inSampleSize) > requiredWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     * This function creates an image file in in pictures folder
     *
     * @return Uri of the file to be saved
     */
    public static Uri getSaveImageFileUri() {
        String saveImageDirPath = Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/"
                + Environment.DIRECTORY_PICTURES;

        new File(saveImageDirPath).mkdirs();
        String timeStamp = java.lang.System.currentTimeMillis() + "";
        File imageFile = new File(saveImageDirPath + "IMG_" + timeStamp + ".jpg");
        return Uri.fromFile(imageFile);
    }

}
