package com.example.ankit.em3assignment.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ankit.em3assignment.R;
import com.example.ankit.em3assignment.adapters.PhotosAdapter;

import java.util.ArrayList;
import java.util.List;

public class PhotosFragment extends Fragment {

    private PhotosAdapter mPhotosAdapter;
    private List<String> mPhotoPaths;

    public static final String KEY_REQUIRED_WIDTH = "KEY_REQUIRED_WIDTH";
    public static final String KEY_REQUIRED_HEIGHT = "KEY_REQUIRED_HEIGHT";

    /*
    * always use static method and arguments to create fragment instance
    * otherwise app may crash if fragments are recreated by system
    * */
    public static PhotosFragment newInstance(int requiredWidth, int requiredHeight) {
        PhotosFragment photosFragment = new PhotosFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_REQUIRED_WIDTH, requiredWidth);
        args.putInt(KEY_REQUIRED_HEIGHT, requiredHeight);
        photosFragment.setArguments(args);
        return photosFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //inflate root view
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        //get require width and required height from arguments
        int requiredWidth = getArguments().getInt(KEY_REQUIRED_WIDTH);
        int requiredHeight = getArguments().getInt(KEY_REQUIRED_HEIGHT);

        //setup recycler view
        RecyclerView rvPhotos = (RecyclerView) rootView.findViewById(R.id.rv_photos);
        if (rvPhotos != null) {

            //setup recycler view layout manager
            rvPhotos.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

            //setup adapter for recycler view
            mPhotoPaths = new ArrayList<>();
            mPhotosAdapter = new PhotosAdapter(getActivity(), mPhotoPaths, requiredWidth, requiredHeight);
            rvPhotos.setAdapter(mPhotosAdapter);
        }

        //return root view
        return rootView;
    }

    //this function adds photo uri at 0th location and notifies adapter that item is inserted
    public void addPhoto(String path) {

        //always do a null check in case of fragments as system can destroy and recreate fragments in low memory cases.
        if (mPhotoPaths != null) {
            mPhotoPaths.add(0, path);
            if (mPhotosAdapter != null) {
                mPhotosAdapter.notifyItemInserted(0);
            }
        }
    }
}
