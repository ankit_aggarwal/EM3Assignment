package com.example.ankit.em3assignment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.ankit.em3assignment.R;
import com.example.ankit.em3assignment.asyncTasks.LoadImageTask;

import java.lang.ref.WeakReference;
import java.util.List;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.PhotosHolder> {

    private WeakReference<Context> mContextWeakReference;
    private List<String> mPhotoPaths;
    private int mRequiredWidth;
    private int mRequiredHeight;

    public PhotosAdapter(Context context, List<String> photoPaths, int requiredWidth, int requiredHeight) {
        //keeping context week reference to prevent memory leaks
        mContextWeakReference = new WeakReference<>(context);
        mPhotoPaths = photoPaths;
        mRequiredWidth = requiredWidth;
        mRequiredHeight = requiredHeight;
    }

    @Override
    public PhotosHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = mContextWeakReference.get();

        //if context is not null then inflate view holder and return
        if (context != null) {
            return new PhotosHolder(LayoutInflater.from(context).inflate(R.layout.adapter_photos, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(PhotosHolder holder, int position) {
        holder.setPhoto(mPhotoPaths.get(position), mRequiredWidth, mRequiredHeight);
    }

    @Override
    public int getItemCount() {
        return mPhotoPaths.size();
    }

    public static class PhotosHolder extends RecyclerView.ViewHolder {

        public ImageView ivPhoto;

        public PhotosHolder(View itemView) {
            super(itemView);
            ivPhoto = (ImageView) itemView.findViewById(R.id.iv_photo);
        }

        public void setPhoto(String path, int requiredWidth, int requiredHeight) {
            new LoadImageTask(ivPhoto, path, requiredWidth, requiredHeight).execute();
        }
    }
}
